#ifndef PID_H
#define PID_H
 
#include "main.h"

typedef struct
{
	float vel_P;
	float vel_I;
	float ang_P;
	float ang_D;
	float integral_vel_prev;
	float error_vel_prev;
	float error_ang_prev;
	float output_ramp;
	float output_prev;
	unsigned long vel_timestamp;
	unsigned long ang_timestamp;
}PID;

extern PID velocityPID;
extern PID anglePID;
/******************************************************************************/
void PID_init(void);
float PID_velocity(PID *pid,float error);
float PID_angle(PID *pid,float error);
/******************************************************************************/

#endif

