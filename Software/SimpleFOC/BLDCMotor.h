#ifndef BLDCMotor_H
#define BLDCMotor_H
#include "main.h"
/******************************************************************************/
/**
 *  Direction structure
 */
typedef enum
{
    CW      = 1,  //clockwise
    CCW     = -1, // counter clockwise
    UNKNOWN = 0   //not yet known or invalid state
} Direction;

typedef struct
{
	float power_supply;
	float limit;
	float sensor_align;
}Type_Voltage;
extern Type_Voltage  Voltage;

/******************************************************************************/
extern long sensor_direction;
extern int  pole_pairs;
extern unsigned long open_loop_timestamp;
extern float velocity_limit;
/******************************************************************************/

void Motor_init(void);
void Motor_initFOC(void);
void Voltage_Init(void);
void loopFOC(void);
void move(float new_target);
void setPhaseVoltage(float Uq, float Ud, float angle_el);

int alignSensor(void);
float velocityOpenloop(float target_velocity);
float angleOpenloop(float target_angle);

#endif
