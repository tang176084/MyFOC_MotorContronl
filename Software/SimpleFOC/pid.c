/**
* @file pid.c
* @brief pid控制相关代码
* @version V1.0
* @author HuiTailang
* @date 2023年8月
*/
#include "pid.h"

PID velocityPID;
PID anglePID;

void PID_init(void)
{
	velocityPID.vel_P=0.1;  //0.1
	velocityPID.vel_I=5;    //2
	velocityPID.output_ramp=100;       //output derivative limit [volts/second]
	velocityPID.integral_vel_prev=0;
	velocityPID.error_vel_prev=0;
	velocityPID.output_prev=0;
	velocityPID.vel_timestamp=SysTick->VAL;
	
	anglePID.ang_P=20;
	anglePID.ang_D=0.85;
	anglePID.error_ang_prev=0;
	anglePID.ang_timestamp=SysTick->VAL;
}
/******************************************************************************/
//just P&I is enough,no need D
float PID_velocity(PID *pid,float error)
{
	unsigned long now_us;
	float Ts;
	float proportional,integral,output;
	float output_rate;
	
	now_us = SysTick->VAL;
	if(now_us<pid->vel_timestamp)Ts = (float)(pid->vel_timestamp - now_us)/9*1e-6f;
	else
		Ts = (float)(0xFFFFFF - now_us + pid->vel_timestamp)/9*1e-6f;
	pid->vel_timestamp = now_us;
	if(Ts == 0 || Ts > 0.5) Ts = 1e-3f;
	
	// u(s) = (P + I/s + Ds)e(s)
	// Discrete implementations
	// proportional part 
	// u_p  = P *e(k)
	proportional = pid->vel_P * error;
	// Tustin transform of the integral part
	// u_ik = u_ik_1  + I*Ts/2*(ek + ek_1)
	integral = pid->integral_vel_prev + pid->vel_I*Ts*0.5*(error + pid->error_vel_prev);
	// antiwindup - limit the output
	integral = _constrain(integral, -Voltage.limit, Voltage.limit);
	
	// sum all the components
	output = proportional + integral;
	// antiwindup - limit the output variable
	output = _constrain(output, -Voltage.limit, Voltage.limit);
	
	// limit the acceleration by ramping the output
	output_rate = (output - pid->output_prev)/Ts;
	if(output_rate > pid->output_ramp)
	{
		output = pid->output_prev + pid->output_ramp*Ts;
	}
	else if(output_rate < -pid->output_ramp)
	{
	output = pid->output_prev - pid->output_ramp*Ts;
	}
	// saving for the next pass
	pid->integral_vel_prev = integral;
	pid->output_prev = output;
	pid->error_vel_prev = error;
	
	return output;
}
/******************************************************************************/
//P&D for angle_PID
float PID_angle(PID *pid,float error)
{
	unsigned long now_us;
	float Ts;
	float proportional,derivative,output;
	//float output_rate;
	
	now_us = SysTick->VAL;
	if(now_us<pid->ang_timestamp)Ts = (float)(pid->ang_timestamp - now_us)/9*1e-6f;
	else
		Ts = (float)(0xFFFFFF - now_us + pid->ang_timestamp)/9*1e-6f;
	pid->ang_timestamp = now_us;
	if(Ts == 0 || Ts > 0.5) Ts = 1e-3f;
	
	// u(s) = (P + I/s + Ds)e(s)
	// Discrete implementations
	// proportional part 
	// u_p  = P *e(k)
	proportional = pid->ang_P * error;
	// u_dk = D(ek - ek_1)/Ts
	derivative = pid->ang_D*(error - pid->error_ang_prev)/Ts;
	
	output = proportional + derivative;
	output = _constrain(output, -velocity_limit, velocity_limit);
	
	// limit the acceleration by ramping the output
//	output_rate = (output - output_ang_prev)/Ts;
//	if(output_rate > output_ang_ramp)output = output_ang_prev + output_ang_ramp*Ts;
//	else if(output_rate < -output_ang_ramp)output = output_ang_prev - output_ang_ramp*Ts;
	
	// saving for the next pass
//	output_ang_prev = output;
	pid->error_ang_prev = error;
	
	return output;
}
/******************************************************************************/
/******************************************************************************/

