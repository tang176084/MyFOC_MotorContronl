/**
* @file lowpass_filter.c
* @brief 低通滤波程序
* @version V1.0
* @author HuiTailang
* @date 2023年8月
*/
#include "lowpass_filter.h" 

/******************************************************************************/
#define DEF_CURR_FILTER_Tf 0.005 //!< default currnet filter time constant
#define DEF_VEL_FILTER_Tf  0.005 //!< default velocity filter time constant
/******************************************************************************/
//unsigned long lpf_vel_timestamp;
float y_vel_prev=0;
/******************************************************************************/
/*
float LPF_velocity(float x)
{
	unsigned long now_us;
	float Ts, alpha, y;
	
	now_us = SysTick->VAL;
	if(now_us<lpf_vel_timestamp)Ts = (float)(lpf_vel_timestamp - now_us)/9*1e-6f;
	else
		Ts = (float)(0xFFFFFF - now_us + lpf_vel_timestamp)/9*1e-6f;
	
	lpf_vel_timestamp = now_us;
	if(Ts == 0 || Ts > 0.5) Ts = 1e-3f; 
	
	alpha = DEF_VEL_FILTER_Tf/(DEF_VEL_FILTER_Tf + Ts);
	y = alpha*y_prev + (1.0f - alpha)*x;
	y_prev = y;
	
	return y;
}
*/

/**
  * @brief  低通滤波
  * @param  x 输入速度
  * @retval y  滤波计算后输出的速度
  */ 
float LPF_velocity(float x)
{
	float y = 0.9*y_vel_prev + 0.1*x;
	
	y_vel_prev=y;
	
	return y;
}
/******************************************************************************/
