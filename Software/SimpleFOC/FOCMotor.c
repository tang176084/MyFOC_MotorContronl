/**
* @file FOCMotor.c
* @brief FOC控制核心程序
* @version V1.0
* @author HuiTailang
* @date 2023年8月
*/
#include "FOCMotor.h" 


float electrical_angle;

float current_sp;



DQVoltage_s voltage;
DQCurrent_s current;
ShaftType shaft;

TorqueControlType torque_controller;
MotionControlType controller;

float sensor_offset=0;
float zero_electric_angle;//0电角度

/**
  * @brief  轴角度计算
  * @param  None 
  * @retval None  
  */ 
float shaftAngle(void)
{
	float angle;
	angle =getAngle();
  return sensor_direction*angle - sensor_offset;
}

/**
  * @brief  轴速度计算
  * @param  None 
  * @retval None  
  */ 
float shaftVelocity(void)
{
	float velocity;
	float lowpass_filter;
	velocity =getVelocity();
  lowpass_filter =LPF_velocity(velocity);//速度输入低通滤波
  return sensor_direction*lowpass_filter;
}

/**
  * @brief  电位角计算
  * @param  None 
  * @retval None  
  */ 
float electricalAngle(void)
{
  return _normalizeAngle((shaft.angle + sensor_offset) * pole_pairs - zero_electric_angle);
}
/******************************************************************************/


