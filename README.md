# 自制FOC电机控制板

#### 介绍
自制FOC电机控制板，方案验证可行。程序移植于著名的simpleFOC方案。主控使用STM32F103C8T6，电机驱动芯片使用DRV8313，电机使用2408云台无刷电机，通过AS5600磁编码器实现闭环控制。

![输入图片说明](img/mode.jpg)

![输入图片说明](img/pcb.png)

![输入图片说明](img/Schematic.png)


